import express from "express";
import { DataTypes, Sequelize, fn, col } from "sequelize";
import { config } from "dotenv";

config();

export const sequelize = new Sequelize(
    process.env.DB_NAME,
    process.env.DB_USERNAME,
    process.env.DB_PASSWORD,
    {
        host: process.env.DB_HOST,
        dialect: "mysql"
    }
);

export const Sku = sequelize.define("SKU", {
    id: { type: DataTypes.INTEGER, unique: true, primaryKey: true, autoIncrement: true },
    sku: DataTypes.STRING,
    description: DataTypes.STRING,
    customerDescription: DataTypes.STRING,
    unitCost: DataTypes.DECIMAL,
    unitPrice: DataTypes.DECIMAL,
    hardCost: DataTypes.TINYINT,
    groupid: DataTypes.INTEGER,
}, {
    timestamps: true,
    createdAt: "dateadded",
    updatedAt: "dateupdated",
    freezeTableName: true,
});

export const Configuration = sequelize.define("Configuration", {
    title: DataTypes.STRING,
    quantities: DataTypes.JSON,
}, {
    timestamps: false
})

export const Group = sequelize.define("SKU_Groups", {
    id: { type: DataTypes.INTEGER, unique: true, primaryKey: true, field: "groupid" },
    name: { type: DataTypes.STRING, field: "group_name" }
}, {
    timestamps: false,
    freezeTableName: true
});

Group.hasMany(Sku, {
    sourceKey: "id",
    foreignKey: "groupid",
    as: "Sku"
});
Sku.belongsTo(Group, {
    sourceKey: "id",
    foreignKey: "groupid",
    as: "Group"
});

await sequelize.authenticate();

const app = express();

app.use(express.json());

app.get("/db/sku_grouped", async (request, response) => {
    let groups = await Group.findAll();
    response.json(Object.fromEntries(
        await Promise.all(groups.map(async (group) => (
            [group.name, await group.getSku()]
        )))
    ));
});

app.get("/db/sku", async (request, response) => {
    response.json(await Sku.findAll());
});

app.post("/db/sku/create", async (request, response) => {
    console.debug("CREATE", request.body);
    const model = await Sku.create(request.body);
    response.json(model.id);
});

app.patch("/db/sku/update", async (request, response) => {
    console.debug("UPDATE", request.query.id, request.body);
    await Sku.update(request.body, { where: { id: request.query.id } });
    response.json();
});

app.delete("/db/sku/delete", async (request, response) => {
    console.debug("DELETE", request.query.id);
    await Sku.destroy({ where: { id: request.query.id } });
    response.json();
});

app.get("/db/configuration", async (request, response) => {
    response.json(await Configuration.findAll());
});

app.post("/db/configuration", async (request, response) => {
    response.json(await Configuration.create(request.body))
});

app.patch("/db/configuration", async (request, response) => {
    response.json(await Configuration.update({
        title: request.body.title,
        quantities: fn("JSON_MERGE_PRESERVE",
            col("quantities"),
            JSON.stringify(request.body.quantities)
        )
    }, { where: { id: request.query.id } }))
});

app.listen(3001, () => {
    console.log("-".repeat(15), "Started Express server.", "-".repeat(15));
});
