/*
Step One Notes:
---

- `Select` Data center question:
    * On-premises
    * Choice Cloud
    * Azure
- `Checkbox` Is EUC required? If yes,
    - `Select`
        * Citrix
        * AVD
        * Citrix on AVD
    - `Integer` How many golden images?
    - `Integer` How many users?
- `Checkbox` If Citrix is required **AND** and data center is (on-premises OR Choice Cloud),
    - `Select`
        * Citrix Cloud
        * Traditional (on-premises)
        * Hybrid (both)
- Traditional MSP Question:
    - `Checkbox` Server management
        - `Checkbox` Patching only (as opposed to fully managed)?
        - If yes,
            - `Integer` Quantity
    - `Checkbox` Desktop patching
        - If yes,
            - `Integer` Quantity
    - `Checkbox` Security
        - `Checkbox` Endpoint protection; **DEFAULT**: yes (because we are managing)
            - `Integer` Servers; **DEFAULT**: `users // 8 + MSP.server_management.quantity + (citrix.traditional: 6, citrix.hybrid: 4, citrix.citrix_cloud: 2)`
            - `Integer` Desktops; **DEFAULT**: `MSP.desktop_patching.quantity`
        - `Checkbox` SIEM as a service
        - `Checkbox` Email security
            - `Integer` Quantity; **DEFAULT**: `users`
        - `Checkbox` Keeper
            - `Integer` Instances; **DEFAULT**: `users`
    - `Checkbox` Network management
        - `Integer` Switches quantity
        - `Integer` Firewalls quantity
        - `Integer` Load balancers
- Backup ADB (BCDR):
    - `Select` Tier
        * Tier 1
        * Tier 2
        * Tier 3
    - `Slider` Storage; **RANGE**: 0-48GB
*/

import { useState, cloneElement, useEffect } from "react";
import {
    Box, Card, Checkbox, FormControl, FormControlLabel, FormLabel,
    Radio, RadioGroup, Slider, Stack, TextField, Tooltip, Typography
} from "@mui/material";
import GridViewOutlined from "@mui/icons-material/GridViewOutlined";
import HouseOutlined from "@mui/icons-material/HouseOutlined";
import CloudOutlined from "@mui/icons-material/CloudOutlined";
import StorageOutlinedIcon from "@mui/icons-material/StorageOutlined";
import DesktopWindowsOutlinedIcon from "@mui/icons-material/DesktopWindowsOutlined";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import PasswordOutlinedIcon from "@mui/icons-material/PasswordOutlined";
import EmailOutlinedIcon from "@mui/icons-material/EmailOutlined";
import ExpandOutlinedIcon from "@mui/icons-material/ExpandOutlined";
import LanguageOutlinedIcon from "@mui/icons-material/LanguageOutlined";
import JoinFullOutlinedIcon from "@mui/icons-material/JoinFullOutlined";
import RadarOutlinedIcon from "@mui/icons-material/RadarOutlined";
import "localforage";

/**
 * A container for multiple cards.
 * Has several cool features that appear based on the parameters!
 * @param {Object} parameters Any unknown parameters will be added
 *      directly to the root component.
 * @param {string | null} parameters.title An optional heading for the section.
 * @param {Array} parameters.children The cards. Auto-populated by JSX.
 * @param {boolean} parameters.expanded A state variable or hardcoded value
 *      indicating if the section children are shown. Note that the heading
 *      is always visible. If not present, the section is always expanded.
 * @param {CallableFunction | null} parameters.setExpanded An optional
 *      state setter callable to toggle if the section children are shown.
        If present, a checkbox will be added next to the heading that
 *      toggles if the section is expanded. If not present,
        the section expansion cannot be changed.
 * @param {boolean} parameters.visible An optional boolean indicating if the
 *      entire section, both heading and children, are visible.
 *      If not present, the section is always visible.
 * @param {("horizontal" | "vertical")} parameters.direction A string
 *      indicating the direction of flow from the heading to the children.
 *      `"horizontal"` is the default and should be used for top-level
 *      sections, while `"vertical"` should be used for any section inside
 *      another `CardSection`.
 */
function CardSection({ title, children, expanded, setExpanded, visible, direction = "horizontal", ...rest }) {
    return <Box display={(visible === undefined || visible) ? "grid" : "none"} gap={2} gridColumn="span 3"
        gridTemplateColumns={direction == "horizontal" ? "300px 1fr" : "1fr"} {...rest}>
        <Box sx={{ display: "flex", flexDirection: "column", rowGap: 1 }}>
            {title &&
                <Typography variant="h4">
                    {setExpanded !== undefined &&
                        <Checkbox sx={{ m: "-9px -5px -5px -9px" }}
                            defaultChecked={expanded}
                            onChange={() => setExpanded(!expanded)} />} {title}
                </Typography>}
        </Box>
        <Box display={(expanded === undefined || expanded) ? "grid" : "none"}
            gridTemplateColumns="repeat(3, 1fr)" gap={2}>{children}</Box>
    </Box>;
}

/**
 * A `TextField` that only accepts numbers and is designed to reflect and
 * update a numerical state value.
 * @param {Object} parameters Any unknown parameters will be added
 *      directly to the root component.
 * @param {string} parameters.label The name of the field.
 * @param {number} parameters.state A state variable or hardcoded value
 *      to fill the field with on render.
 * @param {CallableFunction} parameters.setState A state setter callable
 *      to call whenever the field is changed.
 */
function NumberField({ label, state = 0, setState, ...rest }) {
    return <TextField variant="standard" defaultValue={state} sx={{ width: "100%" }}
        type="number" InputProps={{ inputProps: { min: 0 } }} label={label}
        onChange={(event) => setState(parseInt(event.target.value))} {...rest} />;
}

/**
 * A `Checkbox` and relevant elements designed to reflect and update a boolean
 * state value.
 * @param {Object} parameters Any unknown parameters will be added
 *      directly to the root component.
 * @param {string} parameters.label The name of the field.
 * @param {number} parameters.state A state variable or hardcoded value
 *      to fill the field with on render.
 * @param {CallableFunction} parameters.setState A state setter callable
 *      to call whenever the field is changed.
 */
function CheckboxField({ label, state = false, setState, ...rest }) {
    return <FormControlLabel control={<Checkbox defaultChecked={state}
        onChange={(event, value) => setState(value)} />} label={label} {...rest} />;
}

/**
 * A base `Card` with several cool features that appear based on the parameters.
 * @param {Object} parameters Any unknown parameters will be added
 *      directly to the root component.
 * @param {Element} parameters.icon An optional MUI Icon for large display,
 *      and to be the click trigger for any activation behavior.
 * @param {string} parameters.color A CSS color for the icon.
 *      Defaults to grey (#888888).
 * @param {string} parameters.title An optional heading.
 * @param {CallableFunction} parameters.isActive An optional callable that
 *      when called should return a boolean specifying if this card is "active"
 *      or not. When "inactive", the card will appear greyed out. This is
 *      mainly for the subclasses to implement "radio" and "checkbox" functionality.
 *      Defaults to always returning true, making the card always active.
 * @param {CallableFunction} parameters.makeActive An optional callable to be
 *      called whenever the icon is clicked. It should do something to modify
 *      the active state of the card.
 * @param {Array} parameters.children Any contents. Auto-populated by JSX.
 * @param {number} parameters.width An optional integer specifying the grid
 *      span of the card. Default is 1, but larger values can be used to
 *      make the card take up the width of 2 cards, 3, etc.
 */
function ClickableCard({ icon, color = "grey", title, tooltip, isActive = () => true, makeActive, children, width = 1, ...rest }) {
    return (
        <Card variant="outlined" sx={{
            height: "100%", transition: "filter 0.25s ease", gridColumn: `span ${width}`,
            filter: isActive() ? "none" : "grayscale(1) opacity(0.33) brightness(0.85)"
        }} {...rest}>
            <Box sx={{
                display: "flex", p: 3, columnGap: 2,
                alignItems: children ? "start" : "center"
            }}>
                {icon && <Tooltip title={tooltip} placement="top">
                    {cloneElement(icon, {
                        sx: { fontSize: "67px", color: color },
                        onClick: makeActive,
                    })}
                </Tooltip>}
                <Box sx={{ display: "flex", flexDirection: width > 1 ? "row" : "column", columnGap: 3, rowGap: 1 }}>
                    {title && <Typography variant="h2">{title}</Typography>}
                    {children}
                </Box>
            </Box>
        </Card>
    );
}

/**
 * A `ClickableCard` designed to reflect a boolean state value, able to be
 * toggled on and off by clicking its icon.
 * @param {Object} parameters Any unknown parameters will be added
 *      directly to the root component.
 * @param {boolean} parameters.state A state variable to define if
 *      this card is active or not.
 * @param {CallableFunction} parameters.setState A state setter callable
 *      to call when the active state is changed.
 */
function CheckboxCard({ state, setState, ...rest }) {
    return <ClickableCard tooltip={`Click to ${state ? "disable" : "enable"} this product`}
        isActive={() => !!state} makeActive={() => setState(!state)} {...rest} />;
}

/**
 * A `ClickableCard` designed to share a state variable among a few other
 * `ClickableCard`s, able to set the state value to itself by clicking its icon.
 * @param {Object} parameters Any unknown parameters will be added
 *      directly to the root component.
 * @param {boolean} parameters.state A state variable to define if
 *      this card is active or not.
 * @param {CallableFunction} parameters.setState A state setter callable
 *      to call when the active state is changed.
 */
function RadioCard({ state, setState, value, ...rest }) {
    return <ClickableCard tooltip="Click to select this product"
        isActive={() => state == value} makeActive={() => setState(value)} {...rest} />;
}

/**
 * Step One contains user-friendly checkboxes, number fields, and toggleable
 * cards. These set React state variables, which in turn trigger effects that
 * update the IndexedDB offline cache. The cards do not necessarily correlate
 * one-to-one with Sku fields; rather, they set default values that can be
 * tweaked in Step Two. The effects can react to as many fields at once as
 * desired. This means that this step should only be accessible when starting a
 * new configuration, and should not be accessible once advancing to Step Two.
 */
export default function StepOne() {

    // Ideally these could be dynamic.
    const ADP_BANDS = [
        { value: 0, max: 8, label: "A" },
        { value: 9, max: 18, label: "B" },
        { value: 19, max: 28, label: "C" },
        { value: 29, max: 38, label: "D" },
        { value: 39, max: 48, label: "E" },
    ];
    const ADP_TIERS = [
        { value: 1, label: "Tier 1" },
        { value: 2, label: "Tier 2" },
        { value: 3, label: "Tier 3" },
    ];

    // Yes, yes, unfortunately long list. I couldn't find a practical way to
    // abstract out these state variables and make them dynamic.
    // There may be some way to do it with references.
    const [ServerManagementEnabled, setServerManagementEnabled] = useState(false);
    const [DesktopPatchingEnabled, setDesktopPatchingEnabled] = useState(false);
    const [EucEnabled, setEucEnabled] = useState(false);
    const [KeeperEnabled, setKeeperEnabled] = useState(false);
    const [EmailEnabled, setEmailEnabled] = useState(false);
    const [SiemEnabled, setSiemEnabled] = useState(false);
    const [EndpointProtectionEnabled, setEndpointProtectionEnabled] = useState(false);
    const [NetworkManagementEnabled, setNetworkManagementEnabled] = useState(false);
    const [DataCenterOnPremises, setDataCenterOnPremises] = useState(0);
    const [DataCenterChoiceCloud, setDataCenterChoiceCloud] = useState(0);
    const [DataCenterMicrosoftAzure, setDataCenterMicrosoftAzure] = useState(0);
    const [EucUsers, setEucUsers] = useState(0);
    const [EucGoldenImages, setEucGoldenImages] = useState(0);
    const [EucType, setEucType] = useState(0);
    const [CitrixOnPremises, setCitrixOnPremises] = useState(0);
    const [CitrixChoiceCloud, setCitrixChoiceCloud] = useState(0);
    const [ServerManagementQuantity, setServerManagementQuantity] = useState(0);
    const [DesktopPatchingQuantity, setDesktopPatchingQuantity] = useState(0);
    const [ServerPatchingOnly, setServerPatchingOnly] = useState(true);
    const [KeeperInstances, setKeeperInstances] = useState(0);
    const [EmailProtection, setEmailProtection] = useState(0);
    const [EndpointProtectionServers, setEndpointProtectionServers] = useState(0);
    const [EndpointProtectionDesktops, setEndpointProtectionDesktops] = useState(0);
    const [NetworkSwitches, setNetworkSwitches] = useState(0);
    const [NetworkFirewalls, setNetworkFirewalls] = useState(0);
    const [NetworkLoadBalancers, setNetworkLoadBalancers] = useState(0);
    const [adpEnabled, setAdpEnabled] = useState(false);
    const [adpTier, setAdpTier] = useState(1);
    const [adpBand, setAdpBand] = useState(0);

    // Effects that react to changes in state variables and set IndexedDB values
    // This is the core of Step One, where the logic is implemented.
    useEffect(() => {
        if (DesktopPatchingEnabled) {
            localforage.setItem("MS - Windows Desktop", DesktopPatchingQuantity);
        } else {
            localforage.removeItem("MS - Windows Desktop");
        }
        if (ServerManagementEnabled) {
            if (ServerPatchingOnly) {
                localforage.setItem("MS - Windows Server Patching Only", DesktopPatchingQuantity);
            }
        } else {
            localforage.removeItem("MS - Windows Server Patching Only");
        }
    }, [DesktopPatchingEnabled,
        DesktopPatchingQuantity,
        ServerManagementEnabled,
        ServerManagementQuantity,
        ServerPatchingOnly]);
    useEffect(() => {
        if (KeeperEnabled) {
            localforage.setItem("SECaaS - KEP", KeeperInstances);
        } else {
            localforage.removeItem("SECaaS - KEP");
        }
    }, [KeeperEnabled, KeeperInstances]);
    useEffect(() => {
        if (NetworkManagementEnabled) {
            localforage.setItem("MS - Network Switches", NetworkSwitches);
            localforage.setItem("MS - Network Firewalls", NetworkFirewalls);
        } else {
            localforage.removeItem("MS - Network Switches");
            localforage.removeItem("MS - Network Firewalls");
        }
    }, [NetworkManagementEnabled,
        NetworkSwitches,
        NetworkFirewalls,
        NetworkLoadBalancers]);
    useEffect(() => {
        for (const TIER of ADP_TIERS) {
            for (const BAND of ADP_BANDS) {
                if (adpEnabled && adpTier == TIER.value && BAND.value <= adpBand && adpBand <= BAND.max) {
                    localforage.setItem(`CC - ADP - TIER ${TIER.value} - BAND ${BAND.label}`, 1);
                } else {
                    localforage.removeItem(`CC - ADP - TIER ${TIER.value} - BAND ${BAND.label}`);
                }
            }
        }
    }, [adpEnabled, adpTier, adpBand]);
    return (
        <Stack spacing={8}>
            <CardSection title="Data Center Locations">
                <CheckboxCard color="cadetblue" title="On-Premises" icon={<HouseOutlined />} state={DataCenterOnPremises} setState={setDataCenterOnPremises} />
                <CheckboxCard color="steelblue" title="Choice Cloud" icon={<CloudOutlined />} state={DataCenterChoiceCloud} setState={setDataCenterChoiceCloud} />
                <CheckboxCard color="cadetblue" title="Microsoft Azure" icon={<GridViewOutlined />} state={DataCenterMicrosoftAzure} setState={setDataCenterMicrosoftAzure} />
            </CardSection>
            <CardSection title="Assured Data Protection" expanded={adpEnabled} setExpanded={setAdpEnabled}>
                <ClickableCard>
                    <FormControl>
                        <FormLabel id="tier-group-label">Tier</FormLabel>
                        <RadioGroup aria-labelledby="tier-group-label" value={adpTier} onChange={(event, data) => setAdpTier(data)}>
                            {ADP_TIERS.map((TIER) => (<FormControlLabel key={TIER.value} value={TIER.value} control={<Radio />} label={TIER.label} />))}
                        </RadioGroup>
                    </FormControl>
                </ClickableCard>
                <ClickableCard width={2}>
                    <Box sx={{ flexGrow: 1 }}>
                        <FormLabel id="storage-group-label">Storage</FormLabel>
                        <Slider
                            sx={{ mt: "2px", ml: "8px", mr: "16px", width: "100%" }}
                            value={adpBand}
                            onChange={(event, data) => setAdpBand(data)}
                            min={Math.min(...ADP_BANDS.map((BAND) => BAND.value))}
                            max={Math.max(...ADP_BANDS.map((BAND) => BAND.max))}
                            valueLabelFormat={(value) => `${value}TB`}
                            valueLabelDisplay="auto"
                            marks={ADP_BANDS}
                        />
                    </Box>
                </ClickableCard>
            </CardSection>
            <CardSection title="End-User Computing" expanded={EucEnabled} setExpanded={setEucEnabled}>
                <ClickableCard width={3}>
                    <NumberField label="Users" state={EucUsers} setState={setEucUsers} />
                    <NumberField label="Golden Images" state={EucGoldenImages} setState={setEucGoldenImages} />
                </ClickableCard>
                <RadioCard color="steelblue" title="Citrix" icon={<RadarOutlinedIcon />} value="citrix" state={EucType} setState={setEucType} />
                <RadioCard color="cadetblue" title="Azure Virtual Desktop" icon={<GridViewOutlined />} value="avd" state={EucType} setState={setEucType} />
                <RadioCard color="steelblue" title="Citrix on AVD" icon={<JoinFullOutlinedIcon />} value="citrix_on_avd" state={EucType} setState={setEucType} />
                <CardSection title="Citrix Locations" direction="vertical" visible={EucType === "citrix" && (DataCenterOnPremises || DataCenterChoiceCloud)}>
                    <CheckboxCard color="steelblue" title="On-Premises" icon={<HouseOutlined />} state={CitrixOnPremises} setState={setCitrixOnPremises} />
                    <CheckboxCard color="cadetblue" title="Citrix Cloud" icon={<CloudOutlined />} state={CitrixChoiceCloud} setState={setCitrixChoiceCloud} />
                </CardSection>
            </CardSection>
            <CardSection title="Traditional MSP">
                <CheckboxCard color="cadetblue" title="Server Management" icon={<StorageOutlinedIcon />} state={ServerManagementEnabled} setState={setServerManagementEnabled}>
                    <NumberField label="Quantity" state={ServerManagementQuantity} setState={setServerManagementQuantity} />
                    <CheckboxField label="Patching Only" state={ServerPatchingOnly} setState={setServerPatchingOnly} />
                </CheckboxCard>
                <CheckboxCard color="steelblue" title="Desktop Patching" icon={<DesktopWindowsOutlinedIcon />} state={DesktopPatchingEnabled} setState={setDesktopPatchingEnabled}>
                    <NumberField label="Quantity" state={DesktopPatchingQuantity} setState={setDesktopPatchingQuantity} />
                </CheckboxCard>
            </CardSection>
            <CardSection title="Security">
                <CheckboxCard color="cadetblue" title="Keeper" icon={<PasswordOutlinedIcon />} state={KeeperEnabled} setState={setKeeperEnabled}>
                    <NumberField label="Instances" state={KeeperInstances} setState={setKeeperInstances} />
                </CheckboxCard>
                <CheckboxCard color="steelblue" title="Email" icon={<EmailOutlinedIcon />} state={EmailEnabled} setState={setEmailEnabled}>
                    <NumberField label="Quantity" state={EmailProtection} setState={setEmailProtection} />
                </CheckboxCard>
                <CheckboxCard color="steelblue" title="SIEM as a Service" icon={<LockOutlinedIcon />} state={SiemEnabled} setState={setSiemEnabled} />
                <CheckboxCard color="cadetblue" title="Endpoint Protection" defaultValue={true} icon={<ExpandOutlinedIcon />} state={EndpointProtectionEnabled} setState={setEndpointProtectionEnabled}>
                    <NumberField label="Servers" state={EndpointProtectionServers} setState={setEndpointProtectionServers} />
                    <NumberField label="Desktops" state={EndpointProtectionDesktops} setState={setEndpointProtectionDesktops} />
                </CheckboxCard>
                <CheckboxCard color="steelblue" title="Network Management" icon={<LanguageOutlinedIcon />} state={NetworkManagementEnabled} setState={setNetworkManagementEnabled}>
                    <NumberField label="Switches Quantity" state={NetworkSwitches} setState={setNetworkSwitches} />
                    <NumberField label="Firewalls Quantity" state={NetworkFirewalls} setState={setNetworkFirewalls} />
                    <NumberField label="Load Balancers" state={NetworkLoadBalancers} setState={setNetworkLoadBalancers} />
                </CheckboxCard>
            </CardSection>
        </Stack>
    );
}
