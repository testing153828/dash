import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { Grid, Typography, List, ListItem, ListItemText, TextField, Button, Card, CardContent } from '@mui/material';

const TicketDetails = () => {
  const { ticketId } = useParams();
  const [ticketData, setTicketData] = useState(null);
  const [ticketNotes, setTicketNotes] = useState([]);
  const [newNote, setNewNote] = useState('');

  useEffect(() => {
    fetch(`http://10.100.10.152:5000/cwapi/ticket/${ticketId}`)
      .then((response) => response.json())
      .then((data) => setTicketData(data))
      .catch((error) => {
        console.error('Error fetching ticket data:', error);
        setTicketData(null);
      });

    fetch(`http://10.100.10.152:5000/cwapi/ticketNotes/${ticketId}`)
      .then((response) => response.json())
      .then((data) => setTicketNotes(data))
      .catch((error) => {
        console.error('Error fetching ticket notes:', error);
        setTicketNotes([]);
      });
  }, [ticketId]);

  const handleNewNoteChange = (event) => {
    setNewNote(event.target.value);
  };

  const handleAddNote = () => {
    const noteData = { notes: newNote };

    fetch(`http://10.100.10.152:5000/cwapi/ticketNotes/${ticketId}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: Object.keys(noteData)
        .map(key => encodeURIComponent(key) + '=' + encodeURIComponent(noteData[key]))
        .join('&'),
    })
      .then((response) => response.json())
      .then((data) => {
        setTicketNotes([...ticketNotes, data]);
        setNewNote('');
      })
      .catch((error) => {
        console.error('Error adding new note:', error);
      });
  };

  if (!ticketData) {
    return <p>Loading ticket information...</p>;
  }

  return (
    <Grid container spacing={4}>
      <Grid item xs={12} md={6}>
        <Card>
          <CardContent>
            <Typography variant="h5" style={{ marginBottom: '1rem' }}>Company Details</Typography>
            <Grid container spacing={2}>
              <Grid item xs={6}>
                <Typography variant="body1">Company: {ticketData.company?.name || 'N/A'}</Typography>
                <Typography variant="body1">Contact Name: {ticketData.contact?.name || 'N/A'}</Typography>
                <Typography variant="body1">Contact Phone Number: {ticketData.contactPhoneNumber || 'N/A'}</Typography>
                <Typography variant="body1">Contact Email Address: {ticketData.contactEmailAddress || 'N/A'}</Typography>
                <Typography variant="body1">Site Name: {ticketData.siteName || 'N/A'}</Typography>
              </Grid>
              <Grid item xs={6}>
                <Typography variant="body1">Address: {ticketData.addressLine1 || 'N/A'}</Typography>
                <Typography variant="body1">City: {ticketData.city || 'N/A'}</Typography>
                <Typography variant="body1">State: {ticketData.stateIdentifier || 'N/A'}</Typography>
                <Typography variant="body1">Zip: {ticketData.zip || 'N/A'}</Typography>
                <Typography variant="body1">Country: {ticketData.country?.name || 'N/A'}</Typography>
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      </Grid>
      <Grid item xs={12} md={6}>
        <Card>
          <CardContent>
            <Typography variant="h5" style={{ marginBottom: '1rem' }}>Ticket Details</Typography>
            <Typography variant="body1">Summary: {ticketData.summary}</Typography>
            <Typography variant="body1">Board: {ticketData.board.name}</Typography>
            <Typography variant="body1">Status: {ticketData.status.name}</Typography>
            <Typography variant="body1">Priority: {ticketData.priority.name}</Typography>
            <Typography variant="body1">Work Type: {ticketData.workType.name}</Typography>
          </CardContent>
        </Card>
      </Grid>
      <Grid item xs={12}>
        <Card style={{ maxHeight: '300px', overflowY: 'scroll' }}>
          <CardContent>
            <Typography variant="h5" style={{ marginBottom: '1rem' }}>Ticket Notes</Typography>
            <List>
              {ticketNotes.map((note) => (
                <ListItem key={note.id}>
                  <ListItemText primary={note.text} secondary={`Created By: ${note.createdBy}`} />
                </ListItem>
              ))}
            </List>
          </CardContent>
        </Card>
      </Grid>
      <Grid item xs={12}>
        <Card>
          <CardContent>
            <Typography variant="h5" style={{ marginBottom: '1rem' }}>Add New Note</Typography>
            <TextField
              label="New Note"
              variant="outlined"
              fullWidth
              multiline
              rows={4}
              value={newNote}
              onChange={handleNewNoteChange}
              sx={{ marginBottom: 2 }}
            />
            <Button variant="contained" onClick={handleAddNote}>
              Add Note
            </Button>
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  );
};

export default TicketDetails;