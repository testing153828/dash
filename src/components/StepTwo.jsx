/*
Step Two Notes:
---

If the data centers are on-premises, we don't need to worry about CPU or memory.
But if they are on Choice Cloud or Azure, we need to enter numbers for those servers.

The Azure Management & Infrastructure cost needs to be set manually. The management cost should be a 10% upcharge from Azure cost
*/

import { useState, useEffect } from "react";
import { Box, TextField } from "@mui/material";
import "localforage";

/**
 * This is abstracted out in case I want to change how it is implemented.
 * for instance using `<table>`. Currently this uses a simple div block layout.
 */
function Rows({ children, ...rest }) {
    return <Box mb={6} {...rest}>{children}</Box>;
}

/**
 * This is abstracted out in case I want to change how it is implemented, for
 * instance using `<tr>`. Currently this uses a horizontal flexbox.
 * @param {Object} parameters Any unknown parameters will be added directly to
 *      the root component.
 * @param {Array} parameters.children The cells. Auto-populated by JSX.
 */
function Row({ children, ...rest }) {
    return <Box display="flex" width="100%" py={0.75} borderBottom="1px solid grey" borderColor="grey.900" {...rest}>{children}</Box>;
}

/**
 * This subclasses `Row`, and it is abstracted out in case I want to change how
 * `Row` is implemented, for instance using `<tr>`. Make sure to keep all
 * children of `HeaderRow`s consistent with other `Row`s.
 */
function HeaderRow({ children, ...rest }) {
    return <Row backgroundColor="#f5f5f5" fontSize={12} {...rest}>{children}</Row>;
}

/**
 * This is abstracted out in case I want to change how it is implemented, for
 * instance using `<td>`. Currently this is a flex child.
 * @param {Object} parameters Any unknown parameters will be added directly to
 *      the root component.
 * @param {number} parameters.size The relative width / column span of this cell.
 * @param {Array} parameters.children The cell contents. Auto-populated by JSX.
 */
function Cell({ size, children, ...rest }) {
    return <Box flex={size} px={1} {...rest}>{children}</Box>;
}

/**
 * This is only referenced once, so the only reason this is its own component
 * is because it contains a state variable and an effect that reflect the IndexedDB.
 * @param {Object} parameters 
 * @param {Object} parameters.sku A Sku row returned by the API.
 */
function SkuRow({ sku }) {
    const [quantity, setQuantity] = useState(0);
    useEffect(() => {
        localforage.getItem(sku.sku).then((value) => {
            setQuantity(parseFloat(value || 0));
        });
    }, []);
    return (
        <Row>
            <Cell size={2}>{sku.sku}</Cell>
            <Cell size={3}>{sku.description} / <em>{sku.hardCost ? "Hard" : "Soft"}</em></Cell>
            <Cell size={1}>
                ${sku.unitPrice || <TextField variant="standard" size="small" defaultValue={0} sx={{ width: "50px" }}
                    type="number" InputProps={{ inputProps: { min: 0 } }} />} /
                ${sku.unitCost || <TextField variant="standard" size="small" defaultValue={0} sx={{ width: "50px" }}
                    type="number" InputProps={{ inputProps: { min: 0 } }} />}
            </Cell>
            <Cell size={1} color="steelblue">${(sku.unitPrice * quantity).toFixed(2)} / <em>${(sku.unitCost * quantity).toFixed(2)}</em></Cell>
            <Cell size={"0 0 75px"}>
                <TextField variant="standard" size="small" sx={{ width: "100%" }}
                    type="number" InputProps={{ inputProps: { min: 0 } }}
                    value={quantity} onChange={(event) => {
                        setQuantity(event.target.value);
                        localforage.setItem(sku.sku, parseFloat(event.target.value));
                    }} />
            </Cell>
        </Row>
    );
}

/**
 * Step Two contains a table listing every possible Sku row,
 * grouped by their corresponding Groups.
 * @param {Object} parameters 
 * @param {Object} parameters.groups An object, with keys containing the names
 *      of the groups, and values containing arrays of every Sku row.
 */
export default function StepTwo({ groups }) {
    return (
        <Rows>
            {Object.entries(groups).map(([name, skus]) => (
                <Rows key={name}>
                    <HeaderRow>
                        <Cell size={2}><b>{name}</b></Cell>
                        <Cell size={3}>Description / <em>Hard or Soft</em></Cell>
                        <Cell size={1}>Customer Price / <em>Choice Cost</em></Cell>
                        <Cell size={1}>Customer Total / <em>Choice Total</em></Cell>
                        <Cell size={"0 0 75px"}><em>Quantity</em></Cell>
                    </HeaderRow>
                    {skus.map((sku) => <SkuRow key={sku.sku} sku={sku} />)}
                </Rows>
            ))}
        </Rows>
    );
}